<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.pendaftaran');
    }
    public function signup(Request $request)
    {
        // dd($request->all());
        $namadepan = $request['nama_depan'];
        $namabelakang = $request['nama_Belakang'];
        $gender = $request['JK'];
        $nasional = $request['Nationality'];
        $bahasa = $request ['Language'];
        $pesan = $request['message'];

        return view('page.welcome', compact("namadepan", "namabelakang", "gender", "nasional", "bahasa", "pesan"));
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/daftar', 'AuthController@form');
Route::post('/signup', 'AuthController@signup');

Route::get('/data-table', 'IndexController@table');
Route::group(['middleware' =>['auth']], function(){
    // CRUD cast
    // create
    Route::get('/cast/create', 'castcontroller@create');  //mengarah ke form tambah data
    Route::post('/cast', 'castcontroller@store');  //menyimpan data form ke database table cast

    // Read
    Route::get('/cast', 'castcontroller@index');  //ambil data ke data di tampilkan di blade
    Route::get('/cast/{cast_id}','castcontroller@show'); //Route detail cast

    //Update
    Route::get('/cast/{cast_id}/edit', 'castcontroller@edit'); //Route untuk mengarah ke form edit
    Route::put('/cast/{cast_id}', 'castcontroller@update'); //Route untuk mengarah ke form edit

    //Delete
    Route::delete('/cast/{cast_id}', 'castcontroller@destroy'); //Route delet data berdasarkan id
    Route::get('/home', 'HomeController@index')->name('home');

    //profile
    Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
    ]);
});

Auth::routes();

@extends('layout.master')
@section('title')
Update Profile
@endsection
    @section('content')

    <form method="post" action="/profile/{{$profile->id}}">
      @csrf
      @method('put')
      <div class="form-group">
        <label>Nama User</label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
      </div>
      <div class="form-group">
        <label>Email User</label>
        <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
      </div>

        <div class="form-group">
          <label>Umur</label>
          <input type="number" name="umur" class="form-control" value="{{$profile->umur}}" >
        </div>
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
          <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
          </div>
          @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
          </div>
          @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>

    @endsection
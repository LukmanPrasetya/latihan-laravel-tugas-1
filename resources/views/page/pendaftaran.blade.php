@extends('layout.master')
@section('title')
Halaman pendaftaran
@endsection
@section('content')
    <form action="/signup" method="POST">
    @csrf
    <label>First name : </label><br><br>
    <input type="text" name="nama_depan"><br><br>
    <label>Last name : </label><br><br>
    <input type="text" name="nama_Belakang"><br><br>
    <label>Gender</label><br><br>
    <input value="Male" type="radio" name="JK"> Male<br>
    <input value="Female" type="radio" name="JK"> Female<br><br>
    <label>Nationality</label><br><br>
    <select name="Nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Amerika">Amerika</option>
        <option value="Inggris">Inggris</option>
    </select> <br> <br>
    <label>Language Spoken</label><br><br>
    <input value="Bahasa Indonesia" type="checkbox" name="Language"> Bahasa Indonesia<br>
    <input value="English" type="checkbox" name="Language"> English<br>
    <input value="Other" type="checkbox" name="Language"> Other<br><br>
    <label>Bio</label><br><br>
        <textarea name="message" rows="10" cols="38"></textarea><br>
        <input type="submit" value="Sign Up">

    </form>
    <a href="/">Kembali ke halaman home</a>
@endsection